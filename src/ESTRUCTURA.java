import javax.swing.JOptionPane;


/*
    LISTA SIMPLEMENTE ENLAZADA DE DATOS ENTEROS
 */
class CNodo {

    int dato;
    CNodo siguiente;

    public CNodo() {
        siguiente = null;
    }
}

class CLista {

    CNodo cabeza;

    public CLista() {
        cabeza = null;
    }

    public void InsertarDato(int dat) {
        if (dat < 0) {

            CNodo NuevoNodo;
            CNodo antes, luego;
            NuevoNodo = new CNodo();
            NuevoNodo.dato = dat;
            int ban = 0;
            if (cabeza == null) { //lista esta vacia
                NuevoNodo.siguiente = null;
                cabeza = NuevoNodo;
            } else {
                if (dat < cabeza.dato) //dato va antes de cabeza
                {
                    NuevoNodo.siguiente = cabeza;
                    cabeza = NuevoNodo;
                } else {
                    antes = cabeza;
                    luego = cabeza;
                    while (ban == 0) {
                        if (dat >= luego.dato) {
                            antes = luego;
                            luego = luego.siguiente;
                        }
                        if (luego == null) {
                            ban = 1;
                        } else {
                            if (dat < luego.dato) {
                                ban = 1;
                            }
                        }
                    }
                    antes.siguiente = NuevoNodo;
                    NuevoNodo.siguiente = luego;
                }
            }
        } else {
            System.out.println("ingresar solo enteros negativos:");
        }
    }

    public void EliminarDato(int dat) {
        CNodo antes, luego;
        int ban = 0;
        if (Vacia()) {
            System.out.print("Lista vacía ");
        } else {
            if (dat < cabeza.dato) {
                System.out.print("dato no existe en la lista ");
            } else {
                if (dat == cabeza.dato) {
                    cabeza = cabeza.siguiente;
                } else {
                    antes = cabeza;
                    luego = cabeza;
                    while (ban == 0) {
                        if (dat > luego.dato) {
                            antes = luego;
                            luego = luego.siguiente;
                        } else {
                            ban = 1;
                        }
                        if (luego == null) {
                            ban = 1;
                        } else {
                            if (luego.dato == dat) {
                                ban = 1;
                            }
                        }
                    }
                    if (luego == null) {
                        System.out.print("dato no existe en la Lista ");
                    } else {
                        if (dat == luego.dato) {
                            antes.siguiente = luego.siguiente;
                        } else {
                            System.out.print("dato no existe en la Lista ");
                        }
                    }
                }
            }
        }
    }

    public boolean Vacia() {
        return (cabeza == null);
    }

    public void Imprimir() {
        CNodo Temporal;
        Temporal = cabeza;
        if (!Vacia()) {
            while (Temporal != null) {
                System.out.print(" " + Temporal.dato + " ");
                Temporal = Temporal.siguiente;
            }
            System.out.println("");
        } else {
            System.out.print("Lista vacía");
        }
    }
}

public class ESTRUCTURA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       int opcion=0, el;
        CLista NewLista= new CLista();
        do{
            try{
                opcion= Integer.parseInt(JOptionPane.showInputDialog(null,"1.AGREGAR UN ELEMENTO\n2. MOSTRAR LISTA\n"
                +"3. ELIMINAR ELEMENTO AL INICIO\n"+"4.CONSULTAR LISTA VACIA\n"+"5. SALIR","MENU DE OPCIONES",3));
                switch(opcion){
                    case 1:
                        try{
                            el=Integer.parseInt(JOptionPane.showInputDialog(null, "ingresa el elemento","insertando al inicio",3));
                            if(el <0)
                            {
                            NewLista.InsertarDato(el);
                            }else{
                                JOptionPane.showMessageDialog(null,"error");
                            }
                           
                        }catch(NumberFormatException n){
                        JOptionPane.showMessageDialog(null,"error al ingresar"+n.getMessage());
                        }
                        break;
                    case 2 :
                        NewLista.Imprimir();
                        break;
                    case 3 :
                      /*  NewLista.EliminarDato(el);
                        JOptionPane.showMessageDialog(null,"dato eliminado"+el);
                        break;*/
                    case 4:
                        NewLista.Vacia();
                        break;
                  
                    default :
                        
                
                }
            }catch(Exception e){
                JOptionPane.showMessageDialog(null,"error al ingresar"+e.getMessage());
            
            }
        }while(opcion!=5);
    }
}

